//
//  SearchDetailsVC.swift
//  IMDB-client
//
//  Created by Alexey Kuznetsov on 03/09/2018.
//  Copyright © 2018 Alexey Kuznetsov. All rights reserved.
//

import UIKit

class SearchDetailsVC: UIViewController {
    
    @IBOutlet weak var activityImagePoster: UIActivityIndicatorView!
    @IBOutlet weak var labelPlot: UILabel!
    @IBOutlet weak var labelActors: UILabel!
    @IBOutlet weak var labelDirector: UILabel!
    @IBOutlet weak var labelCountry: UILabel!
    @IBOutlet weak var labelRating: UILabel!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var imagePoster: UIImageView!
    
    var movie: Movie?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadMovieDetails()
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Обновить", style: .done, target: self, action: #selector(loadMovieDetails))
        navigationController?.navigationItem.leftBarButtonItem?.title = ""
    }
    
    @objc func loadMovieDetails() {
        guard let imdbID = movie?.imdbID else {
            showError()
            return
        }
        
        activityImagePoster.isHidden = false
        activityImagePoster.startAnimating()
        API().searchByImdbID(imdbID: imdbID) { (error, movie) in
            if error == nil, let movie = movie {
                self.movie = movie
                self.imagePoster.image = nil
                if let posterUrl = movie.poster, let url = URL(string: posterUrl) {
                    DispatchQueue.global().async { [weak self] in
                        if let data = try? Data(contentsOf: url),
                            let image = UIImage(data: data) {
                                DispatchQueue.main.async {
                                    self?.imagePoster.image = image
                                    self?.activityImagePoster.isHidden = true
                                    self?.activityImagePoster.stopAnimating()
                            }
                        } else {
                            DispatchQueue.main.async {
                                self?.activityImagePoster.isHidden = true
                                self?.activityImagePoster.stopAnimating()
                                self?.imagePoster.image = UIImage(named: "EmptyPoster")
                            }
                        }
                    }
                }
                self.labelPlot.text = "Сюжет: " + (movie.plot ?? "N/A")
                self.labelActors.text = "Актеры: " + (movie.actors ?? "N/A")
                self.labelDirector.text = "Режиссер: " + (movie.director ?? "N/A")
                self.labelCountry.text = "Страна: " + (movie.country ?? "N/A")
                self.labelRating.text = "Рейтинг IMDB: " + (movie.imdbRating ?? "N/A")
                self.labelTitle.text = "Название: " + (movie.title ?? "N/A")
            } else {
                self.activityImagePoster.isHidden = true
                self.activityImagePoster.stopAnimating()
                self.imagePoster.image = nil
                self.showError()
            }
        }
    }
    
    func showError() {
        imagePoster.image = nil
        labelPlot.text = ""
        labelActors.text = ""
        labelDirector.text = ""
        labelCountry.text = ""
        labelRating.text = ""
        labelTitle.text = ""
        
        let alert = UIAlertController(title: "Ошибка !", message: "Что-то пошло не так, попробуйте еще раз.", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        present(alert, animated: true)
    }
    
}

//
//  Network.swift
//  IMDB-client
//
//  Created by Alexey Kuznetsov on 03/09/2018.
//  Copyright © 2018 Alexey Kuznetsov. All rights reserved.
//

import Foundation

struct Error: Decodable {
    var error = ""
    
    enum CodingKeys: String, CodingKey {
        case error = "Error"
    }
}

enum ApiError: String {
    case TooManyResults = "Too many results."
    case MovieNotFound = "Movie not found!"
    case DefaultError = "Something went wrong."
}

class API {
    public let pagesOnRequest = 10 // http://www.omdbapi.com gives only 10 results per request
    private let queue = DispatchQueue(label: "com.somename.low-priority-queue", qos: .utility, attributes: .concurrent, autoreleaseFrequency: .inherit, target: nil)
    private let baseUrl = "http://www.omdbapi.com/"
    private let apikey = "598b9f78"
    
    // MARK: request
    private func sendRequest(parametres: [String: String], callback: @escaping (ApiError?, Data?) -> Void) {
        var body = parametres
        body["apikey"] = apikey
        let url = URL(string: baseUrl)!.appendingQueryParameters(body)
        // setup request
        var urlRequest = URLRequest(url: url)
        urlRequest.setValue(String("application/json"), forHTTPHeaderField: "Content-Type")
    
        self.queue.async {
            // set up the session
            let config = URLSessionConfiguration.default
            let session = URLSession(configuration: config)
            
            // make the request
            let task = session.dataTask(with: urlRequest) {
                (data, response, error) in
                // check for any errors
                guard error == nil else {
                    DispatchQueue.main.async {
                        print("Calling .GET request ends with error")
                        callback(ApiError.DefaultError, nil)
                    }
                    return
                }
                // make sure we got data
                if let responseData = data {
                    DispatchQueue.main.async {
                        callback(nil, responseData)
                    }
                } else {
                    DispatchQueue.main.async {
                        print("Error: did not receive data")
                        callback(ApiError.DefaultError, nil)
                    }
                    return
                }
            }
            task.resume()
            session.finishTasksAndInvalidate()
        }
        
    }
    
    func searchByTitle(title: String, page: Int, callback: @escaping (ApiError?, [Movie], Int) -> Void) {
        
        let parametres = ["s": title, "page": "\(page)"] as [String : String]
        
        sendRequest(parametres: parametres) { (error, data) in
            if let data = data, error == nil {
                do {
                    let searchMovies = try JSONDecoder().decode(SearchMovies.self, from: data)
                    callback(nil, searchMovies.search, Int(searchMovies.totalResults) ?? 0)
                } catch {
                    do {
                        let error = try JSONDecoder().decode(Error.self, from: data)
                        if let apiError = ApiError(rawValue: error.error) {
                            callback(apiError, [], 0)
                        } else {
                            callback(ApiError.DefaultError, [], 0)
                        }
                    } catch {
                        print("Unable to decode results from api")
                        callback(ApiError.DefaultError, [], 0)
                    }
                }
            } else {
                callback(error, [], 0)
            }
        }
    }
    
    func searchByImdbID(imdbID: String, callback: @escaping (ApiError?, Movie?) -> Void) {
        let parametres = ["i": imdbID, "plot": "full"] as [String : String]
        
        sendRequest(parametres: parametres) { (error, data) in
            if let data = data, error == nil {
                do {
                    let movie = try JSONDecoder().decode(Movie.self, from: data)
                    callback(nil, movie)
                } catch {
                    print("Unable to decode movie.")
                    callback(ApiError.DefaultError, nil)
                }
            } else {
                callback(error, nil)
            }
        }
    }
}

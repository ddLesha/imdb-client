//
//  Movie.swift
//  IMDB-client
//
//  Created by Alexey Kuznetsov on 03/09/2018.
//  Copyright © 2018 Alexey Kuznetsov. All rights reserved.
//

import Foundation

class Movie: Decodable {
    /*
      Постер фильма
      Название
      Рейтинг
      Страна
      Режиссер
      Список актеров
      Описание фильма
     */
    var imdbID: String?
    var title: String?
    var imdbRating: String?
    var poster: String?
    var country: String?
    var director: String?
    var actors: String?
    var plot: String?
    
    enum CodingKeys: String, CodingKey {
        case imdbID
        case title = "Title"
        case imdbRating
        case poster = "Poster"
        case country = "Country"
        case director = "Director"
        case actors = "Actors"
        case plot = "Plot"
    }
}

class SearchMovies: Decodable {
    var search = [Movie]()
    var totalResults = "0"
    
    enum CodingKeys: String, CodingKey {
        case search = "Search"
        case totalResults
    }
}


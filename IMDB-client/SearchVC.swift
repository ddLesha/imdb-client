//
//  SearchVC.swift
//  IMDB-client
//
//  Created by Alexey Kuznetsov on 03/09/2018.
//  Copyright © 2018 Alexey Kuznetsov. All rights reserved.
//

import UIKit

class SearchVC: UIViewController, UISearchBarDelegate {
    
    var searchResults = [Movie]()
    var totalMovies = 0
    var titleForSearch = ""
    var loadingInProgress = false
    
    @IBOutlet weak var labelSearchResult: UILabel!
    @IBOutlet weak var activity: UIActivityIndicatorView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 100
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        searchBar.resignFirstResponder()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        tableView.isHidden = true
        activity.isHidden = false
        activity.startAnimating()
        labelSearchResult.isHidden = true
        labelSearchResult.text = ""
        
        guard searchText.trimmingCharacters(in: .whitespaces) != "" else {
            activity.stopAnimating()
            activity.isHidden = true
            labelSearchResult.isHidden = false
            labelSearchResult.text = "Введите название фильма"
            return
        }
        
        //remove starting and ending spaces
        var title = searchText
        while title.last == " " {
            _ = title.removeLast()
        }
        while title.first == " " {
            _ = title.removeFirst()
        }
        
        API().searchByTitle(title: title, page: 1) { (error, movies, total) in
            if error == nil {
                self.searchResults = movies
                self.totalMovies = total
                self.titleForSearch = title
                self.tableView.reloadData()
                self.labelSearchResult.isHidden = true
                self.tableView.isHidden = false
                self.activity.stopAnimating()
                self.activity.isHidden = true
            } else {
                switch error! {
                case .MovieNotFound:
                    self.labelSearchResult.text = "Не нашлось фильмов с таким названием."
                case .TooManyResults:
                    self.labelSearchResult.text = "Слишком много совпадений, добавьте еще букв."
                case .DefaultError:
                    self.labelSearchResult.text = "Что-то пошло не так. Попробуйте еще раз."
                }
                self.activity.stopAnimating()
                self.activity.isHidden = true
                self.labelSearchResult.isHidden = false
            }
        }
    }

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        searchBar.resignFirstResponder()
    }
}

extension SearchVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let movie = searchResults[indexPath.row]
        let searchDetailsVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SearchDetailsVC") as! SearchDetailsVC
        searchDetailsVC.movie = movie
        navigationController?.pushViewController(searchDetailsVC, animated: true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchResults.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SearchCell", for: indexPath) as! SearchCell
        cell.setupCell(title: searchResults[indexPath.row].title ?? "N/A")
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if searchResults.count < self.totalMovies, indexPath.row == searchResults.count - API().pagesOnRequest/2, loadingInProgress == false {
            self.loadingInProgress = true
            let page = self.searchResults.count / API().pagesOnRequest + 1
            API().searchByTitle(title: self.titleForSearch, page: page) { (error, movies, total) in
                self.searchResults.append(contentsOf: movies)
                self.tableView.reloadData()
                self.loadingInProgress = false
            }
        }
    }
}

class SearchCell: UITableViewCell {
    @IBOutlet weak var labelTitle: UILabel!
    
    func setupCell(title: String) {
        labelTitle.text = title
    }
}
